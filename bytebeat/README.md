bytebeat
========

algorithmic symphonies from reverse polish notation

targetting emfcamp 2016 badge tilda mk3


install
-------

needs modified firmware (compiled with dac support), these instructions
modified from those at: https://badge.emfcamp.org/wiki/TiLDA_MK3/build

set up variable to bytebeat sources (directory containing this readme):

    export bytebeat=/path/to/bytebeat/

debian packages for the cross-compiler exist, no need for ppa:

    sudo apt-get install gcc-arm-none-eabi

getting the source:

    git clone --recursive https://github.com/emfcamp/micropython.git
    cd micropython
    git checkout tilda-master

apply dac-enabling patches:

    git checkout -b enable-dac
    git am $bytebeat/000*.patch

build:

    make -C stmhal BOARD=STM32L475_EMFBADGE

flash to tilde in dfu mode (press joystick center with rear reset button):

    make -C stmhal BOARD=STM32L475_EMFBADGE deploy    # untested by me...

you can also reflash (this method tested by me) with the update.py from
https://badge.emfcamp.org/wiki/TiLDA_MK3/Firmware_Update
but you need to patch it so it doesn't automatically download fresh firmware
from the internet and delete it afterwards:

    cd stmhal/build-STM32L475_EMFBADGE
    wget https://update.badge.emfcamp.org/update.py
    patch -p1 < $bytebeat/update-py*.patch
    python update.py

you need to let the badge connect to wifi and download the base system now.

finally, mount the badge and copy the program:

    export badge=/mnt/badge
    sudo mkdir -p $badge
    sudo mount /dev/sdwhatever $badge
    sudo mkdir -p $badge/apps/claude~bytebeat/
    sudo cp $bytebeat/main.py $badge/apps/claude~bytebeat/main.py

alternatively, you can run with pyboard.py to send over usb one-shot (useful
for development), see:
https://badge.emfcamp.org/wiki/TiLDA_MK3/Get_Started#5._Run_a_whole_file_of_code


controls
--------

joystick left/right to navigate (@ cursor shows current selection for A/B press)

joystick center press to delete previous item

hold button A with accelerometer to insert new items on release

hold button B with accelerometer to modify previous item on release

small tilts choose operators (including t)

large tilts choose values (8bit hex)

menu button to quit


electronics
-----------

audio out on pin X6 PB2 (8bit, 8kHz, 0V-3.3V), connect to minijack ring
(with 100uF AC coupling capacitor?)

connect X 0V pin to minijack sleeve (with resistor?)

problems with mains hum due to touching pcb ground, consider making a case
