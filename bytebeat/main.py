### Author: Claude Heiland-Allen
### Description: Live coding byte beat synth
### Category: Music
### License: MIT
### Appname : ByteBeat
 
import buttons
import imu
import ugfx
import pyb
import array
import math

code = ([">>", "&", "f", "t", "t"], [])
 
buttons.init()
accel = imu.IMU()
ugfx.init()

ugfx.clear(ugfx.BLACK)
ugfx.set_default_font(ugfx.FONT_TITLE)
s = ugfx.Style()
s.set_enabled([ugfx.WHITE, ugfx.WHITE, ugfx.WHITE, ugfx.WHITE])
s.set_pressed([ugfx.WHITE, ugfx.WHITE, ugfx.WHITE, ugfx.WHITE])
s.set_disabled([ugfx.WHITE, ugfx.WHITE, ugfx.WHITE, ugfx.WHITE])
s.set_focus(ugfx.BLACK)
s.set_background(ugfx.BLACK)
ugfx.set_default_style(s)

bytecode = array.array('i', [])

opcodes = { "~": 0x100, "<": 0x200, "=": 0x300, ">": 0x400,  "%": 0x500, "/": 0x600, "*": 0x700, "t": 0x800,   "+": 0xa00, "-": 0xb00, "<<": 0xc00,  ">>": 0xd00, "^": 0xe00, "|": 0xf00, "&": 0x1000 }

@micropython.asm_thumb
def interpret_asm(r0, r1, r2): # t, bytecode array, bytecode array length
	# save stack
	push({r10})
	mov(r10, r13)
	# clear stack underflow safety buffer
	mov(r3, 0)
	push({r3})
	push({r3})
	push({r3})
	push({r3})
	push({r3})
	push({r3})
	push({r3})
	push({r3})
	push({r3})
	push({r3})
	push({r3})
	push({r3})
	# for r3 in [0..length)
	cmp(r3, r2)
	blt(LOOP)
	b(DONE)
	label(LOOP)

	ldr(r4, [r1, 0])
	# r4 contains bytecode (2 bytes)
	add(r1, r1, 4)
	mov(r5, r4)
	mov(r6, 8)
	lsr(r5, r6)
	# r5 contains opcode flag

	# ~
	cmp(r5, 0x1)
	itttt(eq)
	pop({r6})
	mvn(r6, r6)
	push({r6})
	b(NEXT)
	# <
	cmp(r5, 0x2)
	bne(SKIP2)
	pop({r6, r7})
	cmp(r7, r6)
	ite(lt)
	mov(r6, 1)
	mov(r6, 0)
	push({r6})
	b(NEXT)
	label(SKIP2)
	# ==
	cmp(r5, 0x3)
	bne(SKIP3)
	pop({r6, r7})
	cmp(r6, r7)
	ite(eq)
	mov(r6, 1)
	mov(r6, 0)
	push({r6})
	b(NEXT)
	label(SKIP3)
	# >
	cmp(r5, 0x4)
	bne(SKIP4)
	pop({r6, r7})
	cmp(r7, r6)
	ite(gt)
	mov(r6, 1)
	mov(r6, 0)
	push({r6})
	b(NEXT)
	label(SKIP4)
	# %
	# no modulo instruction, so do a % b = a - (a / b) * b
	cmp(r5, 0x5)
	bne(SKIP5)
	pop({r6, r7})
	cmp(r6, 0)
	it(eq)
	mov(r6, 1)
	sdiv(r5, r7, r6) # safe to use r5 as we'll branch to NEXT
	mul(r5, r6) # look into mls() instruction?
	sub(r7, r7, r5)
	push({r7})
	b(NEXT)
	label(SKIP5)
	# /
	cmp(r5, 0x6)
	bne(SKIP6)
	pop({r6, r7})
	cmp(r6, 0)
	it(eq)
	mov(r6, 1)
	sdiv(r7, r7, r6)
	push({r7})
	b(NEXT)
	label(SKIP6)
	# *
	cmp(r5, 0x7)
	itttt(eq)
	pop({r6, r7})
	mul(r6, r7)
	push({r6})
	b(NEXT)
	# t
	cmp(r5, 0x8)
	itt(eq)
	push({r0})
	b(NEXT)
	# 0
	cmp(r5, 0x9)
	itttt(eq)
	mov(r6, 0xFF)
	and_(r4, r6)
	push({r4})
	b(NEXT)
	# +
	cmp(r5, 0xA)
	itttt(eq)
	pop({r6, r7})
	add(r6, r6, r7)
	push({r6})
	b(NEXT)
	# -
	cmp(r5, 0xB)
	itttt(eq)
	pop({r6, r7})
	sub(r7, r7, r6)
	push({r7})
	b(NEXT)
	# <<
	cmp(r5, 0xC)
	itttt(eq)
	pop({r6, r7})
	lsl(r7, r6)
	push({r7})
	b(NEXT)
	# >>
	cmp(r5, 0xD)
	itttt(eq)
	pop({r6, r7})
	asr(r7, r6)
	push({r7})
	b(NEXT)
	# ^
	cmp(r5, 0xE)
	itttt(eq)
	pop({r6, r7})
	eor(r6, r7)
	push({r6})
	b(NEXT)
	# |
	cmp(r5, 0xF)
	itttt(eq)
	pop({r6, r7})
	orr(r6, r7)
	push({r6})
	b(NEXT)
	# &
	cmp(r5, 0x10)
	itttt(eq)
	pop({r6, r7})
	and_(r6, r7)
	push({r6})

	# done
	label(NEXT)
	add(r3, r3, 1)
	cmp(r3, r2)
	blt(LOOP)
	label(DONE)
	pop({r0})
	mov(r3, 0xFF)
	and_(r0, r3)
	# restore stack
	mov(r13, r10)
	pop({r10})

def interpret(bytecode, t):
	# pad with 16x 0 in case of underflow...
	stack = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0]
	n = len(bytecode)
	i = 0
	while i < n:
		b = bytecode[i]
		o = b >> 8
		if   o == 1:
			stack = [~stack[0]] + stack[1:]
		elif o == 2:
			stack = [int(stack[1] < stack[0])] + stack[2:]
		elif o == 3:
			stack = [int(stack[1] == stack[0])] + stack[2:]
		elif o == 4:
			stack = [int(stack[1] > stack[0])] + stack[2:]
		elif o == 5:
			if stack[0] == 0:
				stack = [stack[1] % 1] + stack[2:]
			else:
				stack = [stack[1] % stack[0]] + stack[2:]
		elif o == 6:
			if stack[0] == 0:
				stack = [stack[1] / 1] + stack[2:]
			else:
				stack = [stack[1] / stack[0]] + stack[2:]
		elif o == 7:
			stack = [stack[1] * stack[0]] + stack[2:]
		elif o == 8:
			stack = [t] + stack
		elif o == 9:
			stack = [b & 0xFF] + stack
		elif o == 0xa:
			stack = [stack[1] + stack[0]] + stack[2:]
		elif o == 0xb:
			stack = [stack[1] - stack[0]] + stack[2:]
		elif o == 0xc:
			stack = [stack[1] << stack[0]] + stack[2:]
		elif o == 0xd:
			stack = [stack[1] >> stack[0]] + stack[2:]
		elif o == 0xe:
			stack = [stack[1] ^ stack[0]] + stack[2:]
		elif o == 0xf:
			stack = [stack[1] | stack[0]] + stack[2:]
		elif o == 0x10:
			stack = [stack[1] & stack[0]] + stack[2:]
		i = i + 1
	return stack[0] & 0xff

# double buffering
t = 0
which = 0
block = (bytearray(256), bytearray(256))
def interprets():
	global bytecode
	global t
	global block
	global which
	b = block[which]
	n = len(bytecode)
	i = 0
	while i < 256:
		# b[i] = interpret(bytecode, t + i)
		b[i] = interpret_asm(t + i, bytecode, n)
		i = i + 1
	t = (t + 256) & 0x3fffFFFF

dac = pyb.DAC(2)

def callback(timer):
	global block
	global which
	global dac
	dac.write_timed(block[which], 8192, mode=pyb.DAC.NORMAL)
	which = 1 - which
	interprets()

timer = pyb.Timer(8)
timer.init(freq=32)

def opcode(s):
	global opcodes
	try:
		o = opcodes[s]
	except:
		o = 0x900 | (int("0x" + s) & 0xFF)
	return o


def preprocess(code):
	c = list(reversed(code[0])) + code[1]
	n = len(c)
	i = 0
	while i < n:
		c[i] = opcode(c[i])
		i = i + 1
	return array.array('i', c)

cursor = "@"

def text(code):
	global cursor
	return "\n".join([" ".join(reversed(code[0])), cursor, " ".join(code[1])])

source = ugfx.Label(0, 32, 320, 240 - 64, text(code), justification=ugfx.Label.CENTER)
timeout = 0

def update(code):
	global source
	source.text(text(code))

def left():
	global code
	if len(code[0]) > 0:
		code = (code[0][1:], [code[0][0]] + code[1])
		update(code)

def right():
	global code
	if len(code[1]) > 0:
		code = ([code[1][0]] + code[0], code[1][1:])
		update(code)

def delete():
	global code
	global bytecode
	if len(code[0]) > 0:
		code = (code[0][1:], code[1])
		update(code)
		bytecode = preprocess(code)

choosing = False
inserting = False

cursors = ["~", "<", "=", ">",  "%", "/", "*", "t",  "t", "+", "-", "<<",  ">>", "^", "|", "&" ]

def choose():
	global code
	global cursors
	global cursor
	global accel
	a = accel.get_acceleration()
	x = a['x']
	y = a['y']
	r = x * x + y * y
	p = 0.5 + 0.5 * math.atan2(-y, x) / math.pi
	if r > 0.2:
		cursor = hex(int(0xFF * p + 0.5) & 0xFF)[2:]
	else:
		cursor = cursors[int(0x10 * p + 0.5) & 0xF]
	update(code)

def commit(inserting):
	global code
	global cursor
	global bytecode
	if inserting:
		code = ([cursor] + code[0], code[1])
	elif len(code[0]) > 1:
		code = ([cursor] + code[0][1:], code[1])
	cursor = "@"
	update(code)
	bytecode = preprocess(code)

update(code)
bytecode = preprocess(code)
timer.callback(callback)

while not buttons.is_pressed("BTN_MENU"):
	if choosing and not buttons.is_pressed("BTN_A") and not buttons.is_pressed("BTN_B"):
		choosing = False
		commit(inserting)
		timeout = 2
	if buttons.is_pressed("BTN_A") and not buttons.is_pressed("BTN_B"):
		choosing = True
		inserting = True
		choose()
	if not buttons.is_pressed("BTN_A") and buttons.is_pressed("BTN_B"):
		choosing = True
		inserting = False
		choose()
	if timeout == 0:
		if buttons.is_pressed("JOY_LEFT"):
			left()
			timeout = 2
		if buttons.is_pressed("JOY_RIGHT"):
			right()
			timeout = 2
		if buttons.is_pressed("JOY_CENTER"):
			delete()
			timeout = 2
	else:
		timeout = timeout - 1
	t2 = t
	x = (t2 & 0xFF) + 0x20
	y = (t2 >> 8) & 0x1F
	b = block[which]
	ugfx.stream_start(x, y, 256, 1)
	i = 0
	while i < 256:
		o = b[i]
		c = o | (o << 8)
		ugfx.stream_color(c)
		i = i + 1
	ugfx.stream_stop()
	pyb.wfi()
